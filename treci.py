import matplotlib.pyplot as plt
from read_csv_data import read_csv_data

data = read_csv_data('february_data.csv')

dates = list(data.keys())
positives = [data[date]['positive'] for date in dates]
hospitalized = [data[date]['hospitalized'] for date in dates]

fig, ax = plt.subplots()
ax.plot(dates, positives, label='Pozitivni')
ax.plot(dates, hospitalized, label='Hospitalizovani')

ax.set_xlabel('Datum')
ax.set_ylabel('Broj slučajeva')
ax.set_title('Dnevni rast pozitivnih i hospitalizovanih slučajeva za februar 2021.')

ax.legend()

plt.show()