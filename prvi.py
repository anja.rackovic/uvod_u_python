# Define developers dictionary
developers = {
    "Ana": ["Online store website", "Data analysis"],
    "Bruno": ["Pet app", "Data analysis", "Online store website"],
    "Cecilija": ["Pet app", "Online store website"],
    "Damir": ["Data analysis", "Pet app"]
}

# Create a list of all projects
projects = [project for project_list in developers.values() for project in project_list]

# Create a set of all projects to avoid duplicates
unique_projects = set(projects)

# Create a dictionary of all projects and the programmers who work on them
projects_and_programmers = {}

for programmer, project_list in developers.items():
    for project in project_list:
        projects_and_programmers.setdefault(project, []).append(programmer)

# Create a new list of projects with the prefix "BIXBIT_" added to each name
new_projects = {f"BIXBIT_{project}": programmers for project, programmers in projects_and_programmers.items()}

# Define a function to add new developers and projects
def add_programmer(name, project_list):
    developers[name] = project_list
    return len(developers)

# Example of adding a new programmer
add_programmer("Ema", ["Data analysis", "Online store website"])

# Update projects_and_programmers with new programmer
for project, programmers_list in projects_and_programmers.items():
    if "Ema" in programmers_list:
        programmers_list.append("Ema")

# Define a function to update data about developers (number of working hours and earnings)
def update_programmer_data(hours_worked, earnings):
    for programmer, project_list in developers.items():
        updated_projects = []
        for project in project_list:
            updated_projects.append((project, hours_worked, earnings))
        developers[programmer] = updated_projects

# Define a function to print earnings per hour for each programmer
def print_earnings_per_hour():
    for programmer, project_list in developers.items():
        total_hours = 0
        total_earnings = 0
        for project in project_list:
            hours_worked, earnings = project[1], project[2]
            total_hours += int(hours_worked.replace("h", ""))
            total_earnings += int(earnings)
        if total_hours != 0:
            earnings_per_hour = total_earnings / total_hours
        else:
            earnings_per_hour = 0
        print(f"{programmer}: {earnings_per_hour:.2f}$ per hour")

# Example of updating data about developers
update_programmer_data("160h", 4000)

# Example of printing earnings per hour for each programmer
print_earnings_per_hour()
