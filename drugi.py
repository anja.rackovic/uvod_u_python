import json
import csv
from datetime import datetime

# Load data from JSON file
with open('covid-data.json') as f:
    data = json.load(f)

# Extract data for February 2021 and write to CSV file
february_data = []
for item in data:
    if '202102' in str(item['date']):
        february_data.append({
            'date': datetime.strptime(str(item['date']), '%Y%m%d').strftime('%Y-%m-%d'),
            'positive': item['positiveIncrease'],
            'hospitalized': item['hospitalizedIncrease']
        })

with open('february_data.csv', 'w', newline='') as f:
    writer = csv.DictWriter(f, fieldnames=['date', 'positive', 'hospitalized'])
    writer.writeheader()
    writer.writerows(february_data)

# Calculate hospitalization percentage for a given date
def Calculate_hospitalization_percentage(date_string):
    date = datetime.strptime(date_string, '%Y-%m-%d').date()
    positive_count = 0
    hospitalized_count = 0
    for item in february_data:
        if datetime.strptime(item['date'], '%Y-%m-%d').date() == date:
            positive_count += item['positive']
            hospitalized_count += item['hospitalized']
    
    if positive_count == 0:
        return None
    
    percentage = hospitalized_count / positive_count * 100
    return percentage

result = Calculate_hospitalization_percentage('2021-02-05')
print(result)
